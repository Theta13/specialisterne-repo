Imports System.Collections
Imports System.Text

Module VBModule
      Function GetAMillionLines() As String
      Dim s As String = ""
      For i = 1 To 1000000
           s = s & i.ToString & Environment.NewLine
      Next
      Return s
    End Function

		Function OtherMillionLines() As String
			Dim length As Integer = 1000000
			Dim ret As StringBuilder = new StringBuilder(length * 7)
			For i = 1 To length
           ret.AppendLine(i.ToString())
      Next
			return ret.ToString()
		End Function

    Function AnotherMillionLines() As String
			Dim ints As IEnumerable() = Enumerable.Range(0, 1000000).select(Function(x) (x))
			'return String.Join(Environment.NewLine, ints)
			return ""
		End Function


    Sub Main()
			Console.WriteLine("Test Start:")
      'Dim stop1 as Stopwatch = Stopwatch.StartNew()
			Console.WriteLine(OtherMillionLines())
    	'Console.WriteLine(GetAMillionLines())
    	'stop1.Stop()
    	'Console.WriteLine(Stop1.Ellap)
			Console.WriteLine("Test End")
    End Sub
End Module