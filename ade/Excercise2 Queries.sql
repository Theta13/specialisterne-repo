-- ADE exercise 2 queries

-- http://sqlfiddle.com/#!9/b20593/36

-- Part 1
SELECT pt.*
FROM `PeopleTable` pt
INNER JOIN `ParentChildTable` pct
  on pt.PersonId = pct.ChildId
Where pt.FirstName = 'Marge';

SELECT pct.*
FROM `ParentChildTable` pct
INNER JOIN `PeopleTable` pt
  on pt.PersonId = pct.ChildId
Where pt.FirstName = 'Marge';

Select pt.*
From `PeopleTable` pt
Inner Join (
  SELECT pct.*
  FROM `ParentChildTable` pct
  INNER JOIN (
    SELECT pct2.*
    FROM `ParentChildTable` pct2
    Inner Join `PeopleTable` pt2
    on pt2.PersonId = pct2.ChildId
    Where pt2.FirstName = 'Marge' AND pt2.Surname = 'Simpson'
  ) b on b.ParentId = pct.ParentID
) a on a.ChildId = pt.PersonID

Select pt.*
From `PeopleTable` pt
Inner Join (
  SELECT pct.*
  FROM `ParentChildTable` pct
  INNER JOIN (
    SELECT pct2.*
    FROM `ParentChildTable` pct2
    Inner Join `PeopleTable` pt2
    on pt2.PersonId = pct2.ChildId
    Where pt2.FirstName = 'Marge' AND pt2.Surname = 'Simpson'
  ) b on b.ParentId = pct.ParentID
) a on a.ChildId = pt.PersonID and not (pt.FirstName = 'Marge' And pt.Surname = 'Simpson');


-- Part 2
-- Find every record where the child ID also exists in the ParentId column
SELECT pct.*
FROM `ParentChildTable` pct
INNER JOIN `ParentChildTable` ptc2
on pct.ChildId = ptc2.ParentId
Group by pct.ChildId;
  
Select pt.*
From `PeopleTable` pt
Inner Join (
  -- Get every record for which the ChildId also exists in the ParentId column
  SELECT pct.*
  FROM `ParentChildTable` pct
  INNER JOIN `ParentChildTable` ptc2
  on pct.ChildId = ptc2.ParentId
  Group by pct.ChildId
) pct3 on pct3.ParentId = pt.PersonId;


-- Part 3
Select pt.*
From `PeopleTable` pt
Inner Join (
  -- Get Lisa's aunts and uncles from the parentchildtable.
  SELECT pct.*
  FROM `ParentChildTable` pct
  INNER JOIN (
    -- Get Lisa's grandparents
    SELECT pct2.*
    FROM `ParentChildTable` pct2
    Inner Join (
      -- Get Lisa's Parents
      -- ('Lisa' and 'Simpson' could be parameters)
      SELECT pct3.*
      FROM `ParentChildTable` pct3
      Inner Join `PeopleTable` pt3
      on pt3.PersonId = pct3.ChildId
      Where pt3.FirstName = 'Lisa' AND pt3.Surname = 'Simpson'
    ) c on c.ParentId = pct2.ChildId
  ) b on b.ParentId = pct.ParentID
  AND pct.ChildId not in (
    -- This runs the same query for Lisa's parents as above again.
    -- This time it's being run to filter her parents out of the list
    SELECT pct3.ParentId
    FROM `ParentChildTable` pct3
    Inner Join `PeopleTable` pt3
    on pt3.PersonId = pct3.ChildId
    Where pt3.FirstName = 'Lisa' AND pt3.Surname = 'Simpson'
  )
) a on a.ChildId = pt.PersonID and pt.Gender = 'M'
group by pt.PersonId;
