#!/usr/bin/env python3

import json
import codecs
import argparse

def GetJobCodeJson(path, code):
    """
    Open Employees.json from 'path' and return a list containing all the
    employees who are in the job specified by 'code'

    path - Path to Employees.json.
    code - Job code to select.
    """
    with codecs.open(path, "r", 'utf-8-sig') as f:
        data = json.load(f)
    return [d for d in data if d["JobCode"] == code]

def GetJobAverage(path, code):
    """
    Return the average salary for employees in a specified job code.

    path - Path to employees.json.
    code - Job code to select.
    """
    employees = GetJobCodeJson(path, code)
    ret = 0
    for employee in employees:
        ret += employee["Salary"]
    return ret / len(employees)

def GetJobNameJson(path, name):
    """
    Return the job code for a given job name.

    path - Path to job.json.
    name - Name of the job.
    """
    with codecs.open(path, "r", 'utf-8-sig') as f:
        data = json.load(f)
    for job in data:
        if job["Name"] == name:
            return job["JobCode"]

def GetJobTotal(path1, path2, name):
    """
    Return the number of employees in a given job name.

    path1 - Path to Employees.json.
    path2 - Path to jobs.json.
    name  - Name of the job.
    """
    return len(GetJobCodeJson(path1, GetJobNameJson(path2, name)))

parser = argparse.ArgumentParser(description='Tool for getting jobcodes')

parser.add_argument("path")
parser.add_argument("path2", nargs='?')

parser.add_argument("--code", type=int)

parser.add_argument("--name")

opts = parser.parse_args()

if opts.code is not None:
    print(GetJobAverage(opts.path, opts.code))

if opts.name is not None:
    print(GetJobTotal(opts.path, opts.path2, opts.name))
