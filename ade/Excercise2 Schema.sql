-- borrowed from https://stackoverflow.com/q/7745609/808921

CREATE TABLE IF NOT EXISTS `PeopleTable` (
  `PersonId` int(6) unsigned NOT NULL,
  `FirstName` VARCHAR(45) NOT NULL,
  `Surname` varchar(45) NOT NULL,
  `Gender` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`PersonId`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ParentChildTable` (
  `ParentId` int(6) unsigned NOT NULL,
  `ChildId` int(6) unsigned NOT NULL,
  PRIMARY KEY (`ParentId`, `ChildId`)
) DEFAULT CHARSET=utf8;

INSERT INTO `PeopleTable` (`PersonId`, `FirstName`, `Surname`, `Gender`) VALUES
  ('1', 'Marge', 'Simpson', 'F'),
  ('2', 'Homer', 'Simpson', 'M'),
  ('3', 'Bart', 'Simpson', 'M'),
  ('4', 'Lisa', 'Simpson', 'F'),
  ('5', 'Maggie', 'Simpson', 'F'),
  ('6', 'Abe', 'Simpson', 'M'),
  ('7', 'Patty', 'Bouvier', 'F'),
  ('8', 'Selma', 'Bouvier', 'F'),
  ('9', 'Grandma', 'Bouvier', 'F'),
  ('10', 'Herbert', 'Powell', 'M');
  
INSERT INTO `ParentChildTable` (`ParentID`, `ChildId`) VALUES
  ('1', '3'),
  ('2', '3'),
  ('1', '4'),
  ('2', '4'),
  ('1', '5'),
  ('2', '5'),
  ('6', '2'),
  ('9', '1'),
  ('9', '7'),
  ('9', '8'),
  ('6', '10');
