#!/usr/bin/env python3

import pandas as pd

def create_graph(df, filename, kind='scatter', **kwargs):
    ax = pd.DataFrame.plot(df, kind=kind, **kwargs)
    ax.figure.savefig(filename)

def create_graphs(filename, basename="tmp", keys=None):
    df = pd.read_csv(filename)

    colours = []
    for index, row in df.iterrows():
        if row['type'] == "Ghoul":
            colours.append('b')
        elif row['type'] == "Goblin":
            colours.append('r')
        elif row['type'] == "Ghost":
            colours.append('g')
        else:
            raise RuntimeError("Invalid type {}".format(row[type]))

    for i, key1 in enumerate(keys):
        for j, key2 in enumerate(keys[i+1:]):
            create_graph(df, "{0}-{1}-{2}.png".format(basename, i, j),
                         x=key1, y=key2, c=colours)

def graph_colours(filename, basename="colours", types=None, keys=None):
    df = pd.read_csv(filename)

    for t in types:
        # Filter such that only the current type is being considered.
        current = df[df['type'] == t]
        for i, key1 in enumerate(keys):
            for j, key2 in enumerate(keys[i+1:]):
                create_graph(df, "{0}-{1}-{2}.png".format(basename, i, j),
                             x=key1, y=key2, c="color")

keys=[
    "bone_length",
    "rotting_flesh",
    "hair_length",
    "has_soul",
#    "color",
#    "type"
]

types=[
    "Ghoul",
    "Goblin",
    "Ghost",
]

filename="train.csv"

#create_graphs("train.csv", "Test", keys=keys)

graph_colours(filename, "Colours", types=types, keys=keys)
