﻿Public Class Form1
    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        If IsNumeric(TextBox1.Text) Then
            TextBox2.Text = Convert.ToInt32(TextBox1.Text) * 2
        Else
            TextBox2.Text = "Textbox contains non-numerical values"
        End If
    End Sub

    Private Sub DrawShape()
        Dim Colour As Color
        If ComboBox2.Text.Equals("Blue") Then
            Colour = Color.Blue
        ElseIf ComboBox2.Text.Equals("Green") Then
            Colour = Color.Green
        ElseIf ComboBox2.Text.Equals("Red") Then
            Colour = Color.Red
        End If

        Dim graph As Graphics = Graphics.FromHwnd(ActiveForm().Handle)
        Dim abrush As New SolidBrush(Colour)
        Dim rec As Rectangle = New Rectangle(New Point(200, 200), New Size(100, 100))

        If ComboBox1.Text.Equals("Square") Then
            graph.FillRectangle(brush:=abrush, rect:=rec)
        ElseIf ComboBox1.Text.Equals("Circle") Then
            graph.FillEllipse(brush:=abrush, rect:=rec)
        ElseIf ComboBox1.Text.Equals("Triangle") Then
            Dim points As Point() = New Point() {New Point(250, 200), New Point(200, 300), New Point(300, 300)}
            graph.FillPolygon(abrush, points)
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        DrawShape()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged
        DrawShape()
    End Sub
End Class
