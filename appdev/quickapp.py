#!/usr/bin/env python3

# This script exists to inform the user if they have any mergable accounts and
# if so merge them.

import argparse
import pandas

################################################################################

class SuperUsers():
    """This class abstracts away the user data."""

    def __init__(self, source):
        with open (source, mde='r') as data_file:
            self._df = pandas.read_csv(source)

    def get_accounts(self, name="", ID=None, maiden=""):
        ret = []

        person = data.loc[
            (self._df["Client name"] == name)
            & (self._df["Unique ID"] == ID)
            & (self._df["Mother's Maiden Name"] == maiden)
        ]
        if len(person) != 1:
            return None

        account1 = int(
            ''.join(
                c
                for c in person["Superannuation account 1"]
                if c.isdigit()
            )
        )

        if account1 <= 6000 and person["Active 1"] == "Y":

################################################################################

def get_input(message, condition):
    while (True):
        value = input(message)
        if (condition(value)):
            return value
        print (f"Invalid input '{value}'.")

################################################################################

def main():
    
